import cv2
import imutils as imutils
import drawsvg
import os


def gray_image(full_path, adjust):
    """
    Converts an image to grayscale, applies a threshold, and saves the resulting image.

    Args:
        full_path (str): The full path to the input image file.
        adjust (int): The threshold value for image binarization.

    Returns:
        str: The full path to the saved grayscale, thresholded image.
    """
    img = cv2.imread(full_path)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(img_gray, adjust, 255, 0)

    folder, file = os.path.split(full_path)

    new_filename = os.path.join(folder, '$' + file)

    cv2.imwrite(new_filename, thresh)
    return new_filename


def recognize_shapes(filename, adjustment):
    """
    Recognizes shapes in an image and yields the result coordinates, width, and height.

    Parameters:
    filename (str): The file path of the image.
    adjustment (int): The adjustment value for image processing.

    Yields:
    tuple: A tuple containing the result coordinates, width, and height of the recognized shape.
    """
    img = cv2.imread(filename)
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ret, thresh = cv2.threshold(img_gray, adjustment, 255, 0)

    contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(contours)

    for c in cnts:
        peri = cv2.arcLength(c, True)
        borders = cv2.approxPolyDP(c, 0.001 * peri, True)
        result_coordinates = []

        for i, b in enumerate(borders):
            x = b[0][0]
            y = b[0][1]
            result_coordinates.append(x)
            result_coordinates.append(y)

        x, y, w, h = cv2.boundingRect(borders)
        yield result_coordinates, w, h


def create_svg(filename, adjustment):
    try:
        picture = None
        for coors, width, height in recognize_shapes(filename, adjustment):
            if picture is None:
                picture = drawsvg.Drawing(width, height, origin=(0, 0))
            else:
                picture.append(drawsvg.Lines(*coors,
                                             close=False,
                                             fill='#ee0000',
                                             stroke=None))

        picture.save_svg(filename + '.svg')
        return filename + '.svg'
    except Exception as e:
        for param in dir(e):
            if not param.startswith("__"):
                print(param, getattr(e, param))
        return None
