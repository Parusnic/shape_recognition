from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QLabel, QToolBar, \
    QComboBox, QMenu, QMenuBar, QAction, QFileDialog, QVBoxLayout, QSlider, \
    QHBoxLayout, QPushButton, QMessageBox
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtCore import Qt
import os
from recognition import gray_image, create_svg

filename = None


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Индивидуальный проект')
        self.setMinimumSize(500, 500)
        self.setWindowIcon(QIcon('icons\\polygon.png'))

        # ИНТЕРФЕЙС ПАНЕЛИ
        main_toolbar = QToolBar()
        self.regimes = QComboBox()
        self.regimes.setMinimumWidth(400)
        self.regimes.activated.connect(self.show_picture)
        main_toolbar.addWidget(self.regimes)
        self.addToolBar(main_toolbar)

        # ИНТЕРФЕЙС МЕНЮ ДЛЯ ВЫБОРА ФАЙЛА
        self.main_menu = QMenuBar()
        self.menu_file = QMenu('File')
        self.menuElem_open = QAction('Open')
        self.menuElem_exit = QAction('Exit')
        self.menuElem_open.setIcon(QIcon('icons\\open.png'))
        self.menuElem_exit.setIcon(QIcon('icons\\exit.png'))
        self.menu_file.addAction(self.menuElem_open)
        self.menu_file.addSeparator()
        self.menu_file.addAction(self.menuElem_exit)
        self.main_menu.addMenu(self.menu_file)
        self.setMenuBar(self.main_menu)

        self.menuElem_open.triggered.connect(self.open_file)
        self.menuElem_exit.triggered.connect(self.exit_program)

        # Создание виджета-надписи
        self.label = QLabel()
        self.label.setText('Здесь будет картинка')
        self.label.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.setCentralWidget(self.label)
        self.show()

    def show_result(self, svg_file):
        self.regimes.addItems([filename, svg_file])
        self.show_picture()

    def show_picture(self):
        if self.regimes.currentText() == filename:
            pixmapimage = QPixmap(filename)
        else:
            svg_file = filename + '.svg'
            pixmapimage = QPixmap(svg_file)
        # Подгоняю под размеры моего виджета
        w, h = self.label.width(), self.label.height()
        pixmapimage = pixmapimage.scaled(w, h, Qt.KeepAspectRatio)
        # Показываю картинку в виджете label
        self.label.setPixmap(pixmapimage)

    # МЕТОДЫ ДЛЯ ПУНКТОВ МЕНЮ
    def open_file(self):
        file, _ = QFileDialog.getOpenFileName(None, 'Open File', './', "Image (*.png *.jpg *jpeg)")
        if file:
            global filename
            filename = file

            self.regimes.clear()

            window_settings.show_window()

    @staticmethod
    def exit_program():
        app.exit()


class WindowSettings(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Settings")
        self.setMinimumSize(300, 300)
        self.setWindowIcon(QIcon('icons\\settings.png'))

        layout1 = QVBoxLayout()
        self.image = QLabel()
        self.image.setMinimumSize(200, 200)
        layout1.addWidget(self.image)

        layout2 = QVBoxLayout()
        lbl1 = QLabel("Порог распознавания:")
        layout2.addWidget(lbl1, alignment=Qt.AlignBottom | Qt.AlignHCenter)
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setMinimum(0)
        self.slider.setMaximum(255)
        self.slider.setValue(127)
        self.slider.valueChanged.connect(self.change_adjustment)
        layout2.addWidget(self.slider, Qt.AlignTop)
        lbl2 = QLabel('Двигая ползунок влево или вправо, \n вы можете изменить порог распознавания картинки. \n Будет '
                      'распознаны объекты черного цвета.')
        layout2.addWidget(lbl2)

        btn_recognize = QPushButton('Распознать')
        btn_recognize.clicked.connect(self.recognize)
        layout2.addWidget(btn_recognize)
        layout2.setSpacing(20)
        main_layout = QHBoxLayout()
        main_layout.addLayout(layout1)
        main_layout.addLayout(layout2)
        self.setLayout(main_layout)

    def close(self):
        main_window.setEnabled(True)
        self.hide()

    def closeEvent(self, event):
        main_window.setEnabled(True)
        self.hide()

    def change_adjustment(self):
        file = gray_image(filename, self.slider.value())
        self.set_image(file)

    def recognize(self):
        svg_file = create_svg(filename, self.slider.value())
        folder, file = os.path.split(filename)

        new_filename = os.path.join(folder, '$' + file)
        os.remove(new_filename)
        if svg_file is None:
            msg = QMessageBox()
            msg.setWindowTitle('Error')
            msg.setText('Не удалось распознать!')
            msg.setIcon(QMessageBox.Warning)
            msg.setStandardButtons(QMessageBox.Ok)
            msg.exec_()
        else:
            main_window.show_result(svg_file)
            self.close()

    def show_window(self):
        main_window.setEnabled(False)
        file = gray_image(filename, self.slider.value())
        self.set_image(file)
        self.show()

    def set_image(self, image):
        pixmap = QPixmap()
        pixmap.load(image)
        w, h = self.image.width(), self.image.height()
        pixmap = pixmap.scaled(w, h, Qt.KeepAspectRatio)
        self.image.setPixmap(pixmap)


if __name__ == '__main__':
    app = QApplication([])
    main_window = MainWindow()
    window_settings = WindowSettings()
    app.exec()
